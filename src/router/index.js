import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/components/Home'
import City from '@/components/City'

Vue.use(VueRouter)

const routes = [
    {
        path: '/home',
        component: Home
    },
    {
        path: '/city',
        component: City
    },
    {
        path: '/',
        redirect: "/home"
    }
]

export default new VueRouter({
    routes,
    mode: 'history'
})